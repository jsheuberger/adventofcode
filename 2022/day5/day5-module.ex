defmodule AoC.Day5 do

  def stack_message(rows, multi_crates \\ false) do
    # split stack diagram from instructions
    stacks = parse_stacks(rows)

    # perform remaining instructions
    Enum.map(rows, &parse_instruction/1)
    |> Enum.reduce(stacks, fn {take, from, to}, s -> update_stacks(take, from, to, s, multi_crates) end)
    # get message from top crates
    |> take_top_crates()
  end

  defp parse_stacks(rows) do
    lines = Stream.take_while(rows, fn row -> String.length(row) != 0 end)
    |> Enum.reverse()

    cols = Enum.take(lines, 1)
    |> List.first()
    |> String.split()
    |> List.last()
    |> String.to_integer()

    acc = List.duplicate([], cols)
    |> List.to_tuple()

    Enum.drop(lines, 1)
    |> Enum.reduce(acc, fn line, acc -> add_stack_line(acc, line, cols) end)
  end

  defp add_stack_line(acc, line, cols) do
    Range.new(1, 4*cols, 4)
    |> Enum.with_index()
    |> Enum.map(fn {i, j} -> {j, String.at(line, i)} end)
    |> Enum.filter(fn {_i, x} -> x != " " end)
    |> Enum.reduce(acc, fn {index, letter}, acc -> put_elem(acc, index, elem(acc, index) ++ [letter]) end)
  end

  defp parse_instruction(instruction) do
    Regex.run(~r/^move ([[:digit:]]+) from ([[:digit:]]+) to ([[:digit:]]+)$/, instruction, capture: :all_but_first)
    |> Enum.map(&String.to_integer/1)
    |> List.to_tuple()
  end

  defp update_stacks(take, from, to, stacks, multi_crates) do
    {remain, moved} = Enum.split(elem(stacks, from-1), -take)
    moved = unless multi_crates do
      Enum.reverse(moved)
    else
      moved
    end

    put_elem(stacks, to-1, elem(stacks, to-1) ++ moved)
    |> put_elem(from-1, remain)

  end

  defp take_top_crates(stacks) do
    Tuple.to_list(stacks)
    |> Enum.map(&List.last/1)
    |> Enum.join()
  end
end
