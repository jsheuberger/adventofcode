ExUnit.start()

defmodule AoC.Day5Test do
  use ExUnit.Case, async: true

  import AoC.Base
  import AoC.Day5

  describe "Day 5 assignment" do

    test "Part 1 should return CMZ" do
      assert "CMZ" == stack_message(stream_from_file("example"))
    end

    test "PArt 2 should return MCD" do
      assert "MCD" == stack_message(stream_from_file("example"), true)
    end
  end
end
