import AoC.Base
import AoC.Day5

input = stream_from_file("input")

part1 = stack_message(input)
IO.puts("part 1: #{part1}" )

input = stream_from_file("input")
part2 = stack_message(input, true)
IO.puts("part 2: #{part2}")
