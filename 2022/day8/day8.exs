import AoC.Base
import AoC.Day8

input = enum_from_file("input")

part1 = visible_trees(input)
IO.puts("part 1: #{part1}" )

part2 = highest_scenic_score(input)
IO.puts("part 2: #{part2}" )
