defmodule AoC.Day8 do

  def visible_trees(rows) do
    forest = parse_forest(rows)
    # mark visible trees
    visible(forest)
    |> Enum.uniq_by(fn %{id: id} -> id end)
    |> Enum.count()
  end

  def highest_scenic_score(rows) do
    forest = parse_forest(rows)

    # count scenic score for each tree
    scenic_scores(forest)
    |> Enum.max()
  end

  defp parse_forest(lines) do
    lines
    |> Enum.reduce({0, []}, fn row, {count, trees} -> {count+1, trees ++ parse_trees(row, count)} end)
    |> then(fn {rows, trees} -> %{rows: rows, cols: div(length(trees), rows), trees: trees} end)
  end

  defp parse_trees(line, row) do
    line
    |> String.graphemes()
    |> Enum.map(&String.to_integer/1)
    |> Enum.with_index()
    |> Enum.map(fn {height, col} -> %{id: {row, col}, height: height} end)
  end

  defp visible(%{rows: row_size, cols: col_size, trees: trees}) do
    row_trees = for r <- Range.new(0, row_size-1), do: Enum.slice(trees, Range.new(r*row_size, r*row_size + (col_size-1)))

    # [0,5,10,15,20], [1,6,11,16,21], ...
    col_trees = for c <- Range.new(0, col_size-1) do
      for r <- Range.new(0, row_size-1) do
        r*(row_size) + c
      end
      |> Enum.map(fn index -> Enum.at(trees, index) end)
    end

    row_trees ++ Enum.map(row_trees, &Enum.reverse/1) ++ col_trees ++ Enum.map(col_trees, &Enum.reverse/1)
    |> Enum.map(&visible_in/1)
    |> List.flatten()
  end

  defp visible_in(line) do
    line
    |> Enum.reduce({-1, []}, fn tree, {last_height, trees} -> if tree.height > last_height, do: {tree.height, trees ++ [tree]}, else: {last_height, trees} end)
    |> elem(1)
  end

  defp scenic_scores(%{rows: row_size, cols: col_size, trees: trees}) do
    # map scenic score for each tree
    trees
    # |> Enum.filter(fn %{id: id} -> id == {9,9} end)
    |> Enum.map(fn %{id: {x,y}, height: h} ->
        # handle all directions
        [{-1,0}, {0,-1}, {1,0}, {0,1}]
        |> Enum.map(fn {dir_x, dir_y} ->
            visible_trees_from({x,y}, h, trees, {dir_x, dir_y}, {row_size, col_size})
          end)
        # |> IO.inspect()
        |> Enum.reduce(fn x, acc -> x*acc end)
      end)
  end

  defp visible_trees_from({x,y}, height, trees, {dx, dy}, {row_size, col_size}) do
    {x1,y1} = {x+dx, y+dy}

    next_tree = Enum.at(trees, tree_index(x1,y1, row_size))

    cond do
      x1 < 0 || y1 < 0 || x1 >= row_size || y1 >= col_size -> 0
      next_tree.height >= height -> 1
      true -> 1 + visible_trees_from({x+dx,y+dy}, height, trees, {dx, dy}, {row_size, col_size})
    end
  end

  defp tree_index(x,y, row_size) do
    x*row_size + y
  end
end
