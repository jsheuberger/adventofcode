ExUnit.start()

defmodule AoC.Day8Test do
  use ExUnit.Case, async: true

  import AoC.Base
  import AoC.Day8

  describe "Day 8 assignment" do
    test "Part 1 should return 21" do
      assert 21 = visible_trees(enum_from_file("example"))
    end

    test "Part 2 should return 8" do
      assert 8 = highest_scenic_score(enum_from_file("example"))
    end
  end
end
