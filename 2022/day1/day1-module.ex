defmodule AoC.Day1 do

  def max_calories(stream, top) do
    Enum.chunk_while(stream, [], &chunk_fun/2, fn acc -> {:cont, acc, []} end)
    |> Enum.map(&Enum.sum/1)
    # Find the top X
    |> Enum.sort(:desc)
    |> Enum.take(top)
    |> Enum.sum()
  end

  defp chunk_fun("", acc) do
    {:cont, acc, []}
  end
  defp chunk_fun(item, acc) do
    {:cont, [ String.to_integer(item) | acc ]}
  end

end
