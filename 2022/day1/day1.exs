import AoC.Base
import AoC.Day1

input = enum_from_file("input")

part1 = max_calories(input, 1)
IO.puts("part 1: #{part1}" )

part2 = max_calories(input, 3)
IO.puts("part 2: #{part2}")
