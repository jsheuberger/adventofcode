ExUnit.start()

defmodule AoC.Day1Test do
  use ExUnit.Case, async: true

  import AoC.Base
  import AoC.Day1

  describe "test day 1 part 1" do
    test "max calories should return 24000" do
      assert 24000 == max_calories(stream_from_file("./example"), 1)
    end
  end

  describe "test day1 part 2" do
    test "max calories should return 45000" do
      assert 45000 == max_calories(stream_from_file("./example"), 3)
    end
  end

end
