defmodule AoC.Day6 do

  def first_marker(line, chars \\ 4) do
    {_seq, index} = Enum.chunk_every(String.to_charlist(line), chars, 1, :discard)
    |> Enum.with_index()
    |> Enum.filter(fn {x,_i} -> chars == MapSet.size(MapSet.new(x)) end)
    |> List.first()

    index+chars
  end
end
