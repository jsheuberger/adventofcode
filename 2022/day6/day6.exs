import AoC.Base
import AoC.Day6

input = enum_from_file("input")
|> List.first()

part1 = first_marker(input, 4)
IO.puts("part 1: #{part1}" )

part2 = first_marker(input, 14)
IO.puts("part 2: #{part2}")
