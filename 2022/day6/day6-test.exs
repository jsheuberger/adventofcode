ExUnit.start()

defmodule AoC.Day6Test do
  use ExUnit.Case, async: true

  import AoC.Base
  import AoC.Day6

  describe "Day 6 assignment" do
    test "Part 1, example 1 should return [7,5,6,10,11]" do
      assert [7,5,6,10,11] == Enum.map(stream_from_file("example"), & first_marker(&1, 4))
    end

    test "Part 2, example 2 should return [19,23,23,29,26]" do
      assert [19,23,23,29,26] == Enum.map(stream_from_file("example"), & first_marker(&1, 14))
    end
  end
end
