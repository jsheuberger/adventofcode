import AoC.Base
import AoC.Day9

input = enum_from_file("input")

part1 = tail_positions(input)
IO.puts("part 1: #{part1}" )

part2 = tail_positions(input, 10)
IO.puts("part 2: #{part2}" )
