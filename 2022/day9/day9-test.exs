ExUnit.start()

defmodule AoC.Day9Test do
  use ExUnit.Case, async: true

  import AoC.Base
  import AoC.Day9

  describe "Day 9 assignment" do
    test "Part 1 should return 13" do
      assert 13 == tail_positions(enum_from_file("example"))
    end

    test "Part 2 should return 1 for example" do
      assert 1 == tail_positions(enum_from_file("example"), 10)
    end

    test "Part 2 should return 36 for example2" do
      assert 36 == tail_positions(enum_from_file("example2"), 10)
    end
  end
end
