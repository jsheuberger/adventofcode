defmodule AoC.Day9 do

  def tail_positions(input, segments \\2) do
    # parse movement
    parse_input(input)
    # process instructions
    |> perform_moves(segments)
    |> Enum.uniq()
    |> Enum.count()
  end

  defp parse_input(rows) do
    rows
    |> Enum.map(&String.split/1)
    |> Enum.map(fn [dir, count] -> {to_dir(dir), String.to_integer(count)} end)
    |> Enum.map(fn {mv, count} -> List.duplicate(mv, count) end)
    |> List.flatten()
  end

  defp to_dir("R"), do: {1,0}
  defp to_dir("L"), do: {-1,0}
  defp to_dir("U"), do: {0,-1}
  defp to_dir("D"), do: {0,1}

  defp perform_moves(moves, segments) do
    #TODO model the segments
    moves
    |> Enum.reduce({List.to_tuple(List.duplicate({0,0}, segments)), [List.to_tuple(List.duplicate({0,0}, segments))]}, fn {dx,dy}, {segment_positions, all_positions} ->
      {x0,y0} = elem(segment_positions, 0)

      Enum.reduce(Range.new(0, segments-2), put_elem(segment_positions, 0, {x0+dx,y0+dy}), fn i, pos ->
        {x1,y1} = elem(pos, i)
        {x2,y2} = segment_update({x1,y1}, elem(pos, i+1))
        put_elem(pos, i+1, {x2,y2})
      end)
      |> Kernel.then(fn pos -> {pos, all_positions ++ [pos]} end)

    end)
    |> elem(1)
    |> Enum.map(fn pos -> elem(pos, segments-1) end)
  end

  defp segment_update({hx, hy}, {tx, ty}) do
    {dx, dy} = {hx - tx, hy - ty}

    cond do
      abs(dx) <= 1 && abs(dy) <= 1 -> {tx, ty}
      dx == 0 && abs(dy) > 1 -> {tx, ty + div(dy, abs(dy))}
      dy == 0 && abs(dx) > 1 -> {tx + div(dx, abs(dx)), ty}
      true -> {tx + div(dx, abs(dx)), ty + div(dy, abs(dy))}
    end
  end
end
