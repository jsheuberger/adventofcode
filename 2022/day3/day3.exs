import AoC.Base
import AoC.Day3

input = enum_from_file("input")

part1 = pack_score(input)
IO.puts("part 1: #{part1}" )

part2 = badge_score(input)
IO.puts("part 2: #{part2}")
