defmodule AoC.Day3Constants do
  def score_map() do
    String.to_charlist("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
    |> Enum.with_index()
    |> Enum.into(%{})
  end
end

defmodule AoC.Day3 do
alias AoC.Day3Constants

  @item_score Day3Constants.score_map()

  def pack_score(rows) do
    Enum.map(rows, &shared_compartment_items/1)
    |> Enum.map(&score_items/1)
    |> Enum.sum()
  end

  def badge_score(rows) do
    Stream.chunk_every(rows, 3)
    |> Enum.map(&shared_pack_items/1)
    |> Enum.map(&score_items/1)
    |> Enum.sum()
  end

  defp shared_compartment_items(pack) do
    {p1, p2} = String.split_at(pack, div(String.length(pack), 2))
    MapSet.to_list(MapSet.intersection(MapSet.new(String.to_charlist(p1)), MapSet.new(String.to_charlist(p2))))
  end

  defp shared_pack_items([p1,p2,p3]) do
    MapSet.new(String.to_charlist(p1))
    |> MapSet.intersection(MapSet.new(String.to_charlist(p2)))
    |> MapSet.intersection(MapSet.new(String.to_charlist(p3)))
    |> MapSet.to_list()
  end

  defp score_items([]), do: 0
  defp score_items([head|tail]) do
    score_item(head) + score_items(tail)
  end

  defp score_item(item) do
    Map.get(@item_score, item) + 1
  end
end
