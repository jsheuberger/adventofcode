ExUnit.start()

defmodule AoC.Day3Test do
  use ExUnit.Case, async: true

  import AoC.Base
  import AoC.Day3

  describe "Day 3 tests" do

    test "Part 1 should return 157" do
      assert 157 == pack_score(stream_from_file("example"))
    end

    test "Part 2 should return 70" do
      assert 70 == badge_score(stream_from_file("example"))
    end
  end

end
