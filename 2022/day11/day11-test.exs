ExUnit.start()

defmodule AoC.Day11Test do
  use ExUnit.Case, async: true

  import AoC.Base
  import AoC.Day11

  describe "Day 11 assignment" do
    test "Part 1 should return 10605" do
      assert 10605 == monkey_business_score(stream_from_file("example"), 20)
    end

    test "Part 2 should return 2713310158" do
      assert 2713310158 == monkey_business_score(stream_from_file("example"), 10000)
    end
  end
end
