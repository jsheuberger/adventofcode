defmodule AoC.Day11 do

  def monkey_business_score(rows, rounds) do
    monkeys = rows
    |> parse_monkeys()

    lcf = monkeys
    |> Tuple.to_list()
    |> Enum.map(fn x -> x.divisor end)
    |> Enum.product()

    [x,y] = Range.new(1, rounds)
    |> Enum.reduce(monkeys, fn x, monkeys ->
      IO.puts("Round: #{x}")
      do_round(monkeys, rounds < 1000, lcf)
    end)
    |> Tuple.to_list()
    |> Enum.map(fn monkey -> monkey.inspects end)
    |> Enum.sort(:desc)
    |> Enum.take(2)

    x*y
  end

  defp parse_monkeys(rows) do
    rows
    |> Stream.chunk_every(7)
    |> Stream.map(fn x -> Enum.take(x, 6) end)
    |> Stream.map(&List.to_tuple/1)
    |> Stream.map(&parse_monkey/1)
    |> Enum.to_list()
    |> List.to_tuple()
  end

  defp parse_monkey({id_string, starting_items_string, operations_string, test_string, true_string, false_string}) do
    id = id(id_string)
    items = items(starting_items_string)
    op = operation(operations_string)
    {target, divisor} = target(test_string, true_string, false_string)

    %{ id: id, items: items, operation: op, target: target, divisor: divisor, inspects: 0}
  end

  defp id(id_string) do
    id_string
    |> String.split()
    |> List.last()
    |> String.slice(0..-2)
    |> String.to_integer()
  end

  defp items(items_string) do
    items_string
    |> String.split(": ")
    |> List.last()
    |> String.split(", ")
    |> Enum.map(&String.to_integer/1)

  end

  defp operation(op_string) do
    Regex.run(~r/^.+: new = (old|[0-9]+) ([+\-*]) (old|[0-9]+)/, op_string, capture: :all_but_first)
    |> then(fn [a, op, b] -> op_fn(op,a,b) end)
  end

  defp target(test_string, true_string, false_string) do
    divisor = last_as_int(test_string)
    true_target = last_as_int(true_string)
    false_target = last_as_int(false_string)

    {fn x -> if rem(x, divisor) == 0, do: true_target, else: false_target end, divisor}
  end

  defp op_fn("+", a, b) do
    arg_a = arg_fn(a);
    arg_b = arg_fn(b)
    fn x -> apply(Kernel, :+, [arg_a.(x), arg_b.(x)]) end
  end
  defp op_fn("-", a, b) do
    arg_a = arg_fn(a);
    arg_b = arg_fn(b)
    fn x -> apply(Kernel, :-, [arg_a.(x), arg_b.(x)]) end
  end
  defp op_fn("*", a, b) do
    arg_a = arg_fn(a);
    arg_b = arg_fn(b)
    fn x -> apply(Kernel, :*, [arg_a.(x), arg_b.(x)]) end
  end
  defp arg_fn("old") do
    fn x -> x end
  end
  defp arg_fn(arg_val) do
    fn _x -> String.to_integer(arg_val) end
  end

  defp last_as_int(x) do
    x
    |> String.split()
    |> List.last()
    |> String.to_integer()
  end

  defp relief(x, false) do
    x
  end
  defp relief(x, true) do
    floor(x/3)
  end

  defp do_round(monkeys, relief, lcf) do
    Range.new(0, tuple_size(monkeys)-1)
    |> Enum.reduce(monkeys, fn i, acc ->
      turn(i, acc, relief, lcf)
    end)
  end
  defp turn(i, monkeys, relief, lcf) do
    monkey = elem(monkeys, i)
    monkey.items
    |> Enum.reduce(monkeys, fn item, monkeys ->
      value = monkey.operation.(item)
      |> relief(relief)
      |> rem(lcf)

      target = monkey.target.(value)

      new_monkey = elem(monkeys, i)
      target_monkey = elem(monkeys, target)

      put_elem(monkeys, i, %{new_monkey | :items => tl(new_monkey.items), :inspects => new_monkey.inspects+1})
      |> put_elem(target, %{target_monkey | :items => target_monkey.items ++ [value]})
    end)
  end
end
