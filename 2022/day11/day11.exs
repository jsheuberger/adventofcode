import AoC.Base
import AoC.Day11

input = stream_from_file("input")

part1 = monkey_business_score(input, 20)
IO.puts("part 1: #{part1}" )

input = stream_from_file("input")
part2 = monkey_business_score(input, 10000)
IO.puts("part 2: #{part2}" )
