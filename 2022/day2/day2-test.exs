ExUnit.start()

defmodule AoC.Day2Test do
  use ExUnit.Case, async: true

  import AoC.Base
  import AoC.Day2

  describe "Day2 Tests" do
    test "Day 2 Part 1 should return 15" do
      assert 15 == total_score(stream_from_file("example"))
    end

    test "Day 2 Part 2 should return 12" do
      assert 12 == total_score2(stream_from_file("example"))
    end
  end

end
