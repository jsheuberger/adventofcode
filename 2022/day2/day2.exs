import AoC.Base
import AoC.Day2

input = enum_from_file("input")

part1 = total_score(input)
IO.puts("part 1: #{part1}" )

part2 = total_score2(input)
IO.puts("part 2: #{part2}")
