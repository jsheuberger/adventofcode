defmodule AoC.Day2 do

  def total_score(rows) do
    Enum.map(rows, fn x -> String.split(x, " ") end)
    |> Enum.map(&row_score/1)
    |> Enum.sum()
  end

  def total_score2(rows) do
    Enum.map(rows, fn x -> String.split(x, " ") end)
    |> Enum.map(&row_score2/1)
    |> Enum.sum()
  end

  defp row_score2([a, "X"]), do: sign_score(loses(decode(a)))
  defp row_score2([a, "Y"]), do: sign_score(decode(a)) + 3
  defp row_score2([a, "Z"]), do: sign_score(wins(decode(a))) + 6

  defp row_score([a, b]) do
    {s1, s2} = {decode(a), decode(b)}
    sign_score(s2) + game_score({s1, s2})
  end

  defp decode(symbol) do
    case symbol do
      x when x in ["A", "X"] -> :rock
      x when x in ["B", "Y"] -> :paper
      x when x in ["C", "Z"] -> :scissors
    end
  end

  defp sign_score(sign) do
    case sign do
      :rock -> 1
      :paper -> 2
      :scissors -> 3
    end
  end

  defp game_score({a, b}) when a == b, do: 3
  defp game_score({:rock, :paper}), do: 6
  defp game_score({:paper, :scissors}), do:  6
  defp game_score({:scissors, :rock}), do: 6
  defp game_score(_x), do: 0

  defp loses(:rock), do: :scissors
  defp loses(:paper), do: :rock
  defp loses(:scissors), do: :paper

  defp wins(:rock), do: :paper
  defp wins(:paper), do: :scissors
  defp wins(:scissors), do: :rock

end
