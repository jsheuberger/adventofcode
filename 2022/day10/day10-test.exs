ExUnit.start()

defmodule AoC.Day10Test do
  use ExUnit.Case, async: true

  @crt_output "##..##..##..##..##..##..##..##..##..##..
###...###...###...###...###...###...###.
####....####....####....####....####....
#####.....#####.....#####.....#####.....
######......######......######......####
#######.......#######.......#######....."

  import AoC.Base
  import AoC.Day10

  describe "Day 10 assignment" do
    test "Part 1 should return 13140" do
      assert 13140 == register_sum(stream_from_file("example"))
    end

    test "Part 2 should return picture" do
      assert @crt_output == draw_crt(stream_from_file("example"))
    end
  end
end
