import AoC.Base
import AoC.Day10

input = stream_from_file("input")

part1 = register_sum(input)
IO.puts("part 1: #{part1}" )

part2 = draw_crt(stream_from_file("input"))
IO.puts("part 2: \n#{part2}" )
