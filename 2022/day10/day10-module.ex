defmodule AoC.Day10 do

  def register_sum(cmd_stream) do
    cmd_stream
    |> Stream.map(&parse_command/1)
    |> Enum.reduce({1, 1, []}, &apply_operation/2)
    |> then(fn x -> elem(x, 2) end)
    |> Enum.filter(fn {cycle, _val} -> rem(cycle, 40) == 20 end)
    |> Enum.map(fn {cycle, val} -> cycle*val end)
    |> Enum.sum()
  end

  def draw_crt(cmd_stream) do
    cmd_stream
    |> Stream.map(&parse_command/1)
    |> Enum.reduce({1, 1, []}, &apply_operation/2)
    |> then(fn x -> elem(x, 2) end)
    |> Enum.map(&draw_pixel/1)
    |> Enum.chunk_every(40)
    |> Enum.join("\n")
  end

  defp parse_command(cmd) do
    cmd
    |> String.split()
    |> then(fn parts ->
      case parts do
        ["noop"] -> {:noop}
        ["addx", val] -> {:addx, String.to_integer(val)}
      end
    end)
  end

  defp apply_operation({:noop}, {r, cycle, vals}) do
    {r, cycle+1, vals ++ [{cycle, r}]}
  end
  defp apply_operation({:addx, x}, {r, cycle, vals}) do
    {r+x, cycle+2, vals ++ [{cycle, r}, {cycle+1, r}]}
  end

  defp draw_pixel({cycle, sprite_pos}) do
    pixel = rem(cycle-1, 40)
    cond do
      pixel < sprite_pos-1 || pixel > sprite_pos+1 -> '.'
      true -> '#'
    end
  end
end
