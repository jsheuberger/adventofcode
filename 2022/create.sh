#! /bin/bash

NAME=$1
NAME_UC="${NAME^}"

mkdir $NAME
touch $NAME/example
touch $NAME/input

cat > $NAME/$NAME.exs << EOF
import AoC.Base
import AoC.${NAME_UC}

input = enum_from_file("input")

part1 = FUNCTION_NAME(input)
IO.puts("part 1: #{part1}" )

part2 = FUNCTION_NAME(input)
IO.puts("part 2: #{part2}" )
EOF

cat > $NAME/$NAME-test.exs << EOF
ExUnit.start()

defmodule AoC.${NAME_UC}Test do
  use ExUnit.Case, async: true

  import AoC.Base
  import AoC.${NAME_UC}

  describe "Day X assignment" do
    test "Part 1 should return XXX" do
      assert false
    end

    test "Part 2 should return XXX" do
      assert false
    end
  end
end
EOF

cat > $NAME/$NAME-module.ex << EOF
defmodule AoC.${NAME_UC} do

end
EOF