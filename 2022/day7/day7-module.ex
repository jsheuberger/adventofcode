defmodule AoC.Day7 do

  def total_size(stream) do
    tree = parse_commands_to_tree(stream)

    dir_sizes(tree)
    |> Enum.filter(fn {_x, size} -> size <= 100000 end)
    |> Enum.map(fn {_x, size} -> size end)
    |> Enum.sum()
  end

  def directory_to_delete(stream) do
    tree = parse_commands_to_tree(stream)

    sizes = dir_sizes(tree)

    {_name, size} = sizes
    |> Enum.find(0, fn {name, _size} -> name == "/" end)

    # required space is 30M
    min_size = 30000000 - (70000000 - size)

    {name, size} = sizes
    |> Enum.filter(fn {_, size} -> size >= min_size end)
    |> Enum.min_by(fn {_, size} -> size end)

    size
  end

  # %{name: "/", children: [%{name: "file.txt", size: 1234}, %{name: "a", children: []}}
  defp parse_commands_to_tree(cmds) do
    stack = Enum.reduce(cmds, [], fn cmd, stack -> parse_command(cmd, stack) end)
    Enum.reduce(Range.new(2,length(stack)), stack, fn _, acc -> parse_down_dir(acc) end)
    |> List.first()
  end

  defp parse_command(cmd, stack) do
    # IO.inspect({cmd, stack}, label: "command")
    cond do
      String.starts_with?(cmd, "$ cd ..") -> parse_down_dir(stack)
      String.starts_with?(cmd, "$ cd ") -> [%{name: String.slice(cmd, 5..-1), children: []}|stack]
      String.starts_with?(cmd, "dir ") -> stack
      String.starts_with?(cmd, "$ ls") -> stack
      true -> parse_file(cmd, stack)
    end
  end

  defp parse_down_dir([]), do: []
  defp parse_down_dir([dir|[parent|stack]]) do
    %{children: children} = parent
    [%{parent | :children => children ++ [dir]}|stack]
  end

  defp parse_file(line, [hd|tail]) do
    [size, name] = String.split(line)
    %{children: children} = hd
    [%{hd | :children => children ++ [%{name: name, size: String.to_integer(size)}]} | tail]
  end

  defp dir_sizes(nil), do: []
  defp dir_sizes(tree) do
    {_size, list} = dir_sizes_helper(tree, 0, [])
    list
  end

  # if a leaf, just return the updated size
  defp dir_sizes_helper(%{size: file_size}, size, list) do
    {size + file_size, list}
  end
  defp dir_sizes_helper(%{name: name, children: children}, size, list) do
    {children_size, child_list} = Enum.reduce(children, {0, list}, fn child, {s, l} -> dir_sizes_helper(child, s, l) end)

    {children_size + size, [{name, children_size}| child_list]}
  end
end
