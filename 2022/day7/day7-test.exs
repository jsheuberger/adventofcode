ExUnit.start()

defmodule AoC.Day7Test do
  use ExUnit.Case, async: true

  import AoC.Base
  import AoC.Day7

  describe "Day 7 assignment" do
    test "Part 1 should return 95437" do
      assert 95437 == total_size(enum_from_file("example"))
    end

    test "Part 2 should return 24933642" do
      assert 24933642 == directory_to_delete(enum_from_file("example"))
    end
  end
end
