import AoC.Base
import AoC.Day7

input = enum_from_file("input")

part1 = total_size(input)
IO.puts("part 1: #{part1}" )

part2 = directory_to_delete(input)
IO.puts("part 2: #{part2}" )
