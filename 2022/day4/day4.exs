import AoC.Base
import AoC.Day4

input = enum_from_file("input")

part1 = fully_contained_pairs(input)
IO.puts("part 1: #{part1}" )

part2 = overlapping_pairs(input)
IO.puts("part 2: #{part2}")
