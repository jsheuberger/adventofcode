ExUnit.start()

defmodule AoC.Day4Test do
  use ExUnit.Case, async: true

  import AoC.Base
  import AoC.Day4

  describe "Day 4 assignment" do

    test "Part 1 should return 2" do
      assert 2 = fully_contained_pairs(stream_from_file("example"))
    end

    test "Part 2 should return 4" do
      assert 4 = overlapping_pairs(stream_from_file("example"))
    end
  end
end
