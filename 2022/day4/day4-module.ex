defmodule AoC.Day4 do

  def fully_contained_pairs(rows) do
    Stream.map(rows, fn x -> String.split(x, ",") end)
    |> Enum.map(&contained_pair/1)
    |> Enum.filter(& &1)
    |> Enum.count()
  end

  def overlapping_pairs(rows) do
    Stream.map(rows, fn x -> String.split(x, ",") end)
    |> Enum.map(&overlapping_pair/1)
    |> Enum.filter(& !&1)
    |> Enum.count()
  end

  defp contained_pair([r1, r2]) do
    [x1,y1] = to_range(r1)
    [x2,y2] = to_range(r2)

    ((x1 <= x2) && (y1 >= y2)) || ((x2 <= x1) && (y2 >= y1))
  end

  defp overlapping_pair([r1,r2]) do
    [x1,y1] = to_range(r1)
    [x2,y2] = to_range(r2)

    ((y1 < x2) || (x1 > y2)) || ((y2 < x1) || (x2 > y1))
  end

  defp to_range(range) do
    String.split(range, "-")
    |> Enum.map(&String.to_integer/1)
  end
end
