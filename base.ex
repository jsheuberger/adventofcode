defmodule AoC.Base do

  def inputStream(eof \\ "\n") do
    IO.stream(:stdio, :line)
    |> Stream.take_while(& &1 != eof)
  end

  def stream_from_file(filename) do
    {:ok, in_file}  = File.open(filename, [:read, :utf8])
    IO.stream(in_file, :line)
    |> Stream.map(fn x -> String.trim(x, "\n") end )
  end

  def enum_from_file(filename) do
    {:ok, in_file}  = File.open(filename, [:read, :utf8])
    IO.stream(in_file, :line)
    |> Stream.map(fn x -> String.trim(x, "\n") end)
    |> Enum.map(& &1)
  end
end
