defmodule AoCDay4 do

  def run() do
    input = AoCBase.inputStream("\t\n")
    draws = parse_draw(Enum.take(input, 2));
    boards = parse_boards(input)

    # %{draw: draw, winner: winner} = bingo_winner(draws, boards)

    # score = board_score(winner);

    # IO.puts("Winning draw: #{draw}, score: #{score}")

    # output = score*draw

    # IO.puts("Output: #{output}")


    %{draw: draw, winner: winner} = bingo_loser(draws, boards)

    score = board_score(winner);

    IO.puts("Losing board: #{draw}, score: #{score}")

    output = score*draw

    IO.puts("Output part 2: #{output}")

  end

  defp parse_draw([numbers | _]) do
    String.trim(numbers)
    |> String.split(",", trim: true)
    |> Enum.map(&String.to_integer/1)
  end

  defp parse_boards(stream) do
    Stream.map(stream, &String.trim/1)
    |> Stream.filter(fn x -> x != "" end)
    |> Stream.chunk_every(5)
    |> Stream.map(&parse_board/1)
    |> Enum.to_list()
  end

  defp parse_board(list) do
    Enum.map(list, &parse_row/1)
  end

  defp parse_row(row) do
    String.split(row)
    |> Enum.map(&String.to_integer/1)
  end

  defp bingo_winner([draw | remaining], boards) do
    updated = boards_after_draw(draw, boards)
    winner = Enum.find(updated, nil, &has_won?/1)
    if winner != nil do
      %{draw: draw, winner: winner}
    else
      bingo_winner(remaining, updated)
    end
  end

  defp bingo_loser([draw | remaining], boards) do
    updated = boards_after_draw(draw, boards)
    winner = Enum.find(updated, nil, &has_won?/1)
    if winner != nil do
      if length(boards) == 1 do
        %{draw: draw, winner: winner}
      else
        pruned = Enum.reject(updated, &has_won?/1)
        bingo_loser(remaining, pruned)
      end
    else
      bingo_loser(remaining, updated)
    end
  end

  defp boards_after_draw(draw, boards) do
    Enum.map(boards, fn board -> Enum.map(board, fn row -> Enum.map(row, fn x when x == draw -> nil; val -> val; end) end) end)
  end

  defp has_won?(board) do
    winning_row = Enum.any?(board, &is_nil_list?/1)
    winning_column = Enum.zip(board)
    |> Enum.map(&Tuple.to_list/1)
    |> Enum.any?(&is_nil_list?/1)

    winning_row || winning_column
  end

  defp is_nil_list?(list) do
    Enum.all?(list, &is_nil/1)
  end

  defp board_score(board) do
    Enum.reduce(board, 0, &row_score/2)
  end

  defp row_score(row, acc) do
    row_sum = Enum.reject(row, &is_nil/1)
    |> Enum.sum()

    acc + row_sum
  end
end

AoCDay4.run()
