defmodule AoCDay5 do

  def run() do
    input = AoCBase.inputStream()

    lines = parse_lines(input)
    |> Enum.to_list()

    filtered = filter_horz_vert(lines)

    count = count_overlapping(filtered, 2)
    IO.puts("Output: #{count} overlapping points (filtered)")

    count = count_overlapping(lines, 2)
    IO.puts("Output: #{count} overlapping points")

  end

  defp parse_lines(stream) do
    Stream.map(stream, &String.trim/1)
    |> Stream.map(&parse_line/1)
  end

  defp parse_line(line) do
    [start_point, end_point] = String.split(line, " -> ")
    |> Enum.map(&parse_point/1)
    %{start: start_point, end: end_point}
  end

  defp parse_point(point_str) do
    [x,y] = String.split(point_str, ",")
    |> Enum.map(&String.to_integer/1)

    %{x: x, y: y}
  end

  defp filter_horz_vert(lines) do
    Enum.reject(lines, fn line -> line.start.x != line.end.x && line.start.y != line.end.y end)
  end

  defp count_overlapping(lines, threshold) do
    points = Enum.reduce(lines, %{}, fn x, acc -> count_helper(x, acc) end)
    Map.values(points)
    |> Enum.flat_map(&Map.values/1)
    |> Enum.filter(fn x -> x >= threshold end)
    |> length

  end

  defp count_helper(%{start: sp, end: ep}, acc) when sp.x == ep.x do
    Range.new(sp.y, ep.y)
    |> Enum.reduce(acc, fn y, acc -> Map.update(acc, sp.x, Map.put(%{}, y, 1), fn ys -> Map.update(ys, y, 1, fn val -> val+1 end) end) end)
  end
  defp count_helper(%{start: sp, end: ep}, acc) when sp.y == ep.y do
    Range.new(sp.x, ep.x)
    |> Enum.reduce(acc, fn x, acc -> Map.update(acc, x, Map.put(%{}, sp.y, 1), fn ys -> Map.update(ys, sp.y, 1, fn val -> val+1 end) end) end)
  end
  defp count_helper(%{start: sp, end: ep}, acc) do
    Range.new(sp.x, ep.x)
    |> Enum.zip(Range.new(sp.y,ep.y))
    |> Enum.reduce(acc, fn {x,y}, acc -> Map.update(acc, x, Map.put(%{}, y, 1), fn ys -> Map.update(ys, y, 1, fn val -> val+1 end) end) end)
  end

end


AoCDay5.run()
