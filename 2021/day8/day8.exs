defmodule AoCDay8 do

  def run() do
    input = AoCBase.inputStream()

    entries = parse_input(input)

    count = count_easy(entries)

    IO.puts("Total #{count} unique segments (2,3,4,7)")

    output = decode_entries(entries)

    IO.puts("Sum of entries is #{output}")

  end

  defp parse_input(stream) do
    Stream.map(stream, fn x -> String.split(String.trim(x), "|") end)
    |> Stream.map(fn [ input | [ output ]] -> {String.split(input, " ", trim: true), String.split(output, " ", trim: true)} end)
    |> Enum.to_list()

  end

  defp count_easy(stream) do
    Enum.map(stream, fn {_input, output} -> output end)
    |> IO.inspect()
    |> Enum.flat_map(fn words -> Enum.filter(words, fn pattern -> Enum.member?([2,3,4,7], String.length(pattern)) end) end)
    |> Enum.reject(&is_nil/1)
    |> length()
  end

  defp decode_entries(entries) do
    Stream.map(entries, &decode_entry/1)
    |> Enum.sum()
  end

  defp decode_entry({input, output}) do
    input = Enum.map(input, fn word -> Enum.sort(String.graphemes(word)) end)

    hint = decipher(input)

    Enum.map(output, fn pattern -> decode(pattern, hint) end)
    |> IO.inspect()
    |> Integer.undigits()
  end

  defp decipher(input) do
    #uniques
    one = Enum.find(input, fn word when length(word) == 2 -> true; _ -> false end)
    four = Enum.find(input, fn word when length(word) == 4 -> true; _ -> false end)
    seven = Enum.find(input, fn word when length(word) == 3 -> true; _ -> false end)
    eight = Enum.find(input, fn word when length(word) == 7 -> true; _ -> false end)

    # 2,3,5
    five_len = Enum.filter(input, fn word when length(word) == 5 -> true; _ -> false end)
    # 0,6,9
    six_len = Enum.filter(input, fn word when length(word) == 6 -> true; _ -> false end)

    #decipher the others
    two = Enum.find(five_len, fn word -> 3 == length(filter_all(word, four)) end)
    three = Enum.find(five_len, fn word ->  3 == length(filter_all(word, one)) end)
    [five] = Enum.reject(five_len, fn x when x == two -> true; x when x == three -> true; _ -> false end)

    zero = Enum.find(six_len, fn word -> 2 == length(filter_all(word, five)) end)
    nine = Enum.find(six_len, fn word -> 1 == length(filter_all(word, three)) end)

    [six] = Enum.reject(six_len, fn x when x == nine -> true; x when x == zero -> true; _ -> false end)

    [zero, one, two, three, four, five, six, seven, eight, nine]
    |> Enum.map(&Enum.join/1)
    |> IO.inspect()

  end

  defp filter_all(word, chars) do
    Enum.reject(word, fn x -> Enum.member?(chars, x) end)
  end

  defp decode(pattern, hint) do
    sorted = String.graphemes(pattern)
    |> Enum.sort()
    |> Enum.join()

    IO.inspect(sorted)

    Enum.find_index(hint, fn x -> x == sorted end)
  end
end

AoCDay8.run()
