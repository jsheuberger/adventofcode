move = fn
  "forward", val, [x,y] -> [x + val, y]
  "down", val, [x,y] -> [x, y + val]
  "up", val, [x,y] -> [x, y - val]
end



[x,y] = IO.stream(:stdio, :line)
|> Stream.take_while(& &1 != "\n")
|> Stream.map(fn x -> String.split(String.trim(x)) end)
|> Stream.map(fn [cmd, val] -> [cmd, String.to_integer(val)] end)
|> Enum.reduce([0, 0], fn [cmd, val], acc -> move.(cmd, val, acc) end)

IO.puts(x*y)
