move = fn
  "forward", val, [x,y,a] -> [x + val, y + a*val, a]
  "down", val, [x,y,a] -> [x, y, a + val]
  "up", val, [x,y,a] -> [x, y, a - val]
end

[x,y,_] = IO.stream(:stdio, :line)
|> Stream.take_while(& &1 != "\n")
|> Stream.map(fn x -> String.split(String.trim(x)) end)
|> Stream.map(fn [cmd, val] -> [cmd, String.to_integer(val)] end)
|> Enum.reduce([0, 0, 0], fn [cmd, val], acc -> move.(cmd, val, acc) end)

IO.puts(x*y)
