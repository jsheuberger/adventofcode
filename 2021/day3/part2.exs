import Bitwise

defmodule AoCDay3 do

  def add_elems([], []) do
    []
  end

  def add_elems([h1 | t1], [h2 | t2]) do
    [ h1 + h2 | add_elems(t1,t2)]
  end

  def oxygen([x], _) do
    x
  end

  def oxygen( list , index) do
    oxygen(filter(list, index), index+1)
  end

  defp filter( list, index) do
    most_common_bits = most_common_bits(list)
    most_common = Enum.at(most_common_bits, index)
    IO.inspect(most_common)
    Enum.filter(list, fn x -> most_common = Enum.at(list, index) end)
  end

  defp most_common_bits(list) do
    %{count: count, list: l} = Enum.reduce(list, %{count: 0, list: nil}, fn x, %{count: count, list: nil} -> %{count: count+1, list: x}; x, %{count: count, list: acc} -> %{count: count+1, list: AoCDay3.add_elems(x,acc)} end)
    Enum.map(l, fn x -> round(x/count) end)
  end
end

output = IO.stream(:stdio, :line)
|> Stream.take_while(& &1 != "\n")
|> Stream.map(fn x -> String.split(String.trim(x), "", trim: true) end) # Split
|> Stream.map(fn x -> Enum.map(x, &String.to_integer/1) end) # To integers
|> Enum.to_list()
|> IO.inspect()

AoCDay3.oxygen(output, 0)
