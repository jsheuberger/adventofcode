import Bitwise

defmodule AoCDay3 do

  def add_elems([], []) do
    []
  end

  def add_elems([h1 | t1], [h2 | t2]) do
    [ h1 + h2 | add_elems(t1,t2)]
  end

end

output = IO.stream(:stdio, :line)
|> Stream.take_while(& &1 != "\n")
|> Stream.map(fn x -> String.split(String.trim(x), "", trim: true) end) # Split
|> Stream.map(fn x -> Enum.map(x, &String.to_integer/1) end) # To integers
|> Enum.reduce(%{count: 0, list: nil}, fn x, %{count: count, list: nil} -> %{count: count+1, list: x}; x, %{count: count, list: acc} -> %{count: count+1, list: AoCDay3.add_elems(x,acc)} end)

bits = Enum.map(output.list, fn x -> round(x/output.count) end)
|> Enum.map(&Integer.to_string/1)
|> Enum.join()

{gamma, _} = Integer.parse(bits, 2)
epsilon = bxor(gamma, elem(Integer.parse(String.duplicate("1",String.length(bits)), 2), 0))

IO.puts(gamma*epsilon)
