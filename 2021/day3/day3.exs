defmodule AoCDay3 do

  def run() do
    input = AoCBase.inputStream()
    diagnostics = parse_diagnostics(input) |> Enum.to_list()
    consumption = power_consumption(diagnostics)
    IO.puts("Power consumption: #{consumption}")
    ls = life_support(diagnostics)
    IO.puts("Life Support: #{ls}")
  end

  defp parse_diagnostics(input) do
    Stream.map(input, &parse_row/1)
  end

  defp parse_row(row) do
    String.split(String.trim(row), "", trim: true)
    |> Enum.map(&String.to_integer/1)
  end

  defp power_consumption(diagnostics) do
    common = Enum.zip(diagnostics)
    |> Enum.map(&Tuple.to_list/1)
    |> Enum.map(&most_common/1)

    gamma = Integer.undigits(common, 2)
    epsilon = Integer.undigits(Enum.map(common, fn x -> 1-x end), 2)

    gamma*epsilon
  end

  defp most_common(row) do
    ratio = Enum.sum(row)/length(row)
    case ratio do
      x when x > 0.5 -> 1
      x when x == 0.5 -> -1
      x when x < 0.5 -> 0
    end
  end

  defp life_support(diagnostics) do
    oxygen = filter_match(diagnostics, 0, &most_common_match/3)
    |> Integer.undigits(2)
    co2 = filter_match(diagnostics, 0, &least_common_match/3)
    |> Integer.undigits(2)

    oxygen*co2
  end

  defp filter_match([x], _index, _matcher) do
    x
  end
  defp filter_match(list, index, matcher) do
    common = Enum.zip(list)
    |> Enum.map(&Tuple.to_list/1)
    |> Enum.map(&most_common/1)

    Enum.filter(list, fn x -> matcher.(x, common, index) end)
    |> filter_match(index+1, matcher)
  end

  defp most_common_match(val, common, index) do
    case Enum.at(common, index) do
      -1 -> Enum.at(val, index) == 1
      x -> Enum.at(val, index) == x
    end
  end

  defp least_common_match(val, common, index) do
    case Enum.at(common, index) do
      -1 ->  Enum.at(val, index) == 0
      x ->  Enum.at(val, index) == 1-x
    end
  end

end


AoCDay3.run()
