defmodule AoCDay7 do

  def run() do
    input = AoCBase.inputStream()

    crabs = parse_input(input)

    %{position: pos, fuel: fuel} = align_crabs(crabs, &distance/2)

    IO.puts("Output: crabs align to #{pos} using #{fuel} fuel")

    %{position: pos, fuel: fuel} = align_crabs(crabs, &distance_triangle/2)

    IO.puts("Output: crabs align to #{pos} using #{fuel} fuel")

  end

  defp parse_input(stream) do
    Stream.map(stream, fn x -> String.split(String.trim(x), ",") end)
    |> Enum.at(0)
    |> Stream.map(&String.to_integer/1)
    |> Enum.to_list()
  end

  defp align_crabs(crabs, distance_fn) do
    {min, max} = Enum.min_max(crabs);
    ranges = Range.new(min, max)
    distances = Enum.map(ranges, fn x -> distance_fn.(crabs, x) end)
    l = max-min;
    avg = Enum.map(distances, fn x -> Enum.sum(x)/l end)

    IO.inspect({ranges, distances, avg})

    smallest = Enum.min(avg)
    index = Enum.find_index(avg, fn x -> smallest == x end)

    %{position: Enum.at(ranges, index), fuel: Enum.sum(Enum.at(distances, index))}
  end

  defp distance(list, point) do
    Enum.map(list, fn x -> abs(point - x) end)
  end

  defp distance_triangle(list, point) do
    Enum.map(list, fn x -> triangular_number(abs(point - x)) end)
  end

  defp triangular_number(x) do
    (x*x + x)/2
  end

end

AoCDay7.run()
