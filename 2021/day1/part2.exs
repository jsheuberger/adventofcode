is_increasing = fn
  nil, _ -> false
  x, y -> x < y
end

IO.stream(:stdio, :line)
|> Stream.take_while(& &1 != "\n")
|> Stream.map(fn x -> String.to_integer(String.trim(x)) end)
|> Stream.chunk_every(3, 1)
|> Stream.reject(fn x -> length(x) != 3 end)
|> Stream.map(fn x -> Enum.sum(x) end)
|> Enum.reduce(%{:last => nil, :count => 0 }, fn x, acc -> %{:last => x, :count => acc.count + (is_increasing.(acc.last, x) && 1 || 0)} end)|> Map.get(:count)
|> IO.puts()
