defmodule AoCDay9 do

  def run() do
    input = AoCBase.inputStream()

    heightmap = parse_input(input)

    IO.inspect(heightmap)

    risk_sum = calculate_risk_sum(heightmap)

    IO.puts("Output: #{risk_sum}")

    
  end

  defp parse_input(stream) do
    list_data = Stream.map(stream, fn x -> Enum.map(String.split(String.trim(x), "", trim: true), &String.to_integer/1) end)
    |> Enum.to_list()

    height = length(list_data)
    width = length(hd(list_data))

    data = List.to_tuple(List.flatten(list_data))

    %{data: data, size: {width, height}}
  end

  defp calculate_risk_sum(heightmap) do
    find_low_points(heightmap)
    |> Enum.map(fn x -> x+1 end)
    |> Enum.sum()
  end

  defp find_low_points(%{data: data, size: {width, height}}) do
    for x <- Range.new(0, width-1),
        y <- Range.new(0, height-1) do
          if is_lowest_point({x,y},{width,height},data), do: elem(data, index(x,y,width))
    end
    |> Enum.reject(&is_nil/1)
  end

  defp is_lowest_point({x,y},{width,height},data) do
    index = index(x,y,width)
    val = elem(data, index)
    (x-1 < 0 || elem(data, index-1) > val) &&
    (x+1 >= width || elem(data, index+1) > val) &&
    (y-1 < 0 || elem(data, index-width) > val) &&
    (y+1 >= height || elem(data, index+width) > val)
  end

  defp index(x,y, width) do
    width*y+x
  end
end


AoCDay9.run()
