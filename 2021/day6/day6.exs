defmodule AoCDay6 do

  def run() do
    input = AoCBase.inputStream()

    fish = parse_input(input)

    days = 256
    output = count_fish(fish, days)

    IO.puts("Output: #{output} fish after #{days} days")
  end


  defp parse_input(stream) do
    Stream.map(stream, fn x -> String.split(String.trim(x), ",") end)
    |> Enum.at(0)
    |> Stream.map(&String.to_integer/1)
    |> Enum.to_list()
  end

  defp count_fish(start, days) do
    buckets = Enum.reduce(start, Tuple.duplicate(0, 9), fn x, acc -> put_elem(acc, x, elem(acc,x)+1) end)
    |> Tuple.to_list()
    count_helper(buckets, days)
  end

  defp count_helper(buckets, 0) do
    Enum.sum(buckets)
  end
  defp count_helper([d0,d1,d2,d3,d4,d5,d6,d7,d8], days_left) do
    new_buckets = [d1, d2, d3, d4, d5, d6, d7+d0, d8, d0]

    IO.inspect(new_buckets)
    count_helper(new_buckets, days_left-1)
  end
end


AoCDay6.run()
